<?php

namespace App\Libs;

class CustomResponse
{
    public static function store($checker, $table, $data = NULL) {
        $data = ($data ?? $checker);
        if ($checker) {
            return response()->json([
                "code" => 200,
                "status" => "Success",
                "data" => $data,
                "message" => "Success create $table data",
            ], 200);
        }

        return response()->json([
            "code" => 500,
            "status" => "Failed",
            "data" => null,
            "message" => "Failed create $table data",
        ], 500);
    }
    
    public static function error($message = "Error")
    {
        return response()->json([
            "code" => 500,
            "status" => "Error",
            "data" => null,
            "message" => $message,
        ], 500);
    }
    
    
    public static function not_found($message = "Data Not Found")
    {
        return response()->json([
            "code" => 404,
            "status" => "Not Found",
            "data" => null,
            "message" => $message,
        ], 404);
    }
    
    public static function failed_send_email()
    {
        return response()->json([
            "code" => 500,
            "status" => "Error",
            "data" => null,
            "message" => "Failed Send Email",
        ], 500);
    }

    public static function success($data, $message = "Success")
    {
        return response()->json([
            "code" => 200,
            "status" => "Success",
            "data" => $data,
            "message" => $message,
        ], 200);
    }
}
