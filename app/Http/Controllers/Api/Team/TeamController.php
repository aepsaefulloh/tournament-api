<?php

namespace App\Http\Controllers\Api\Team;

use DB;
use App\Models\Team;
use App\Models\Users;
use App\Models\Match;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    private $image_destionation = 'storage/images/teams/';

    public function index(Request $request)
    {
        $search = $request->search;
        $skip = $request->skip;
        $take = $request->take;

        $data = Team::select('ID','TEAM_NAME','LOGO',
            DB::raw('(SELECT COUNT(ID) FROM tbl_user WHERE TEAM_ID = tbl_team.ID) AS total_member'))
        ->when($search, function ($query, $search) {
            $query->where('title', 'LIKE', '%'.$search.'%');
        })->where('STATUS','<>',99);

        if ($skip && $take) {
            $data = $data->skip($skip)->take($take);
        }else{
            $data = $data->limit(10);
        }
        
        $data = $data->get();
        
        return self::responseGet($data, "Team");
    }

    public function about(Request $request, $id)
    {
        $skip = $request->skip;
        $take = $request->take;

        $data = Team::select('TEAM_NAME', 'DESCRIPTION', 'LOGO', 'LEADER',
            DB::raw('(SELECT COUNT(ID) FROM tbl_team_member WHERE TEAM_ID = tbl_team.ID) AS total_member'))
        ->where([['ID', $id], ['STATUS','<>',99]])->first();
        
        return self::responseGet($data, "Team detail - About");
    }

    public function member(Request $request, $id)
    {
        $skip = $request->skip;
        $take = $request->take;
        
        $data = DB::table('tbl_team_member')->select('USERNAME', 'STATUS')
        ->where('TEAM_ID', $id);
        
        if ($skip && $take) {
            $data = $data->skip($skip)->take($take);
        }else{
            $data = $data->limit(10);
        }
        
        $data = $data->get();
        
        return self::responseGet($data, "Team detail - Member");
    }

    public function match(Request $request, $id)
    {
        $skip = $request->skip;
        $take = $request->take;
        
        $data = Match::select(DB::raw('DATE(MATCHDATE) AS MATCHDATE'),
            DB::raw("(CASE
                WHEN ISNULL(TOURNAMENT_ID) THEN 'Private Match'
                ELSE (
                SELECT TNAME FROM tbl_tournament WHERE tbl_tournament.ID = TOURNAMENT_ID
                )
            END) AS MATCH_NAME"),DB::raw('(
                SELECT LOGO FROM tbl_game WHERE tbl_game.ID = GAME_ID
            ) AS GAME_LOGO'),DB::raw('(
                SELECT TEAM_NAME FROM tbl_team WHERE tbl_team.ID = PLAYER1
            ) AS FIRST_TEAM_NAME'),DB::raw('(
                SELECT TEAM_NAME FROM tbl_team WHERE tbl_team.ID = PLAYER2
            ) AS SECOND_TEAM_NAME'),DB::raw('(
                SELECT LOGO FROM tbl_team WHERE tbl_team.ID = PLAYER1
            ) AS FIRST_TEAM_LOGO'),DB::raw('(
                SELECT LOGO FROM tbl_team WHERE tbl_team.ID = PLAYER2
            ) AS SECOND_TEAM_LOGO')
        )->where('PLAYER1','=',$id)
        ->orWhere('PLAYER2','=',$id);
        
        if ($skip && $take) {
            $data = $data->skip($skip)->take($take);
        }else{
            $data = $data->limit(10);
        }
        
        $data = $data->get();
        
        return self::responseGet($data, "Team detail - Match");
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'logo' => 'required',
            'location', 'required',
            'team_name' => 'required',
            'description' => 'required',
        ]);

        $leader_id = Users::getIdFromToken($request->header('token'));
        $is_created_team = Users::select('TEAM_ID')->where('ID', $leader_id)->first();

        if ($is_created_team['TEAM_ID']) {
            return response()->json([
                'status' => 422,
                'message' => 'You already created team!',
            ], 422);
        }
        
        if ($request->logo['name'] != '') {
            $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->logo['type'];
            $is_file_saved = $this->save_image($request->logo['name'], $imageName, $this->image_destionation, $request->logo['type']);

            if (isset($is_file_saved['status'])) {
                return response()->json([
                    'status' => 422,
                    'message' => $is_file_saved['message'],
                ], 422);
            }
        }

        $insert = [
            'LEADER' => $leader_id,
            'LOCATION' => $request->location,
            'TEAM_NAME' => $request->team_name,
            'DESCRIPTION' => $request->description,
            'LOGO' => $this->image_destionation.$imageName,
        ];

        $team_id = Team::insertGetId($insert);

        if ($team_id) {
            $store = Users::where('ID', $leader_id)->update([
                'TEAM_ID' => $team_id
            ]);
        
            return self::responseStore($store, "Team");
        }else{
            return response()->json([
                'status' => 500,
                'message' => 'Failed to store teams data',
            ], 500);
        }
    }
}
