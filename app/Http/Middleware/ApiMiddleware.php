<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Master;

class ApiMiddleware
{
    public function __construct()
    {
        $this->master = New Master;
        $this->table = 'tbl_token';
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         // Searching Token
         if ($request->header('token') != '') {
            $token = $this->master->result_filtering($this->table, [['TOKEN', $request->header('token')]], ['TOKEN', 'STATUS']);
        }
        
        // Cek Token 
        if (isset($token) && $token->TOKEN == $request->header('token') && $token->STATUS == 1) {
            return $next($request);
        } else {
            return response(['status'=>401,'message'=>'Unauthorized.'], 401);   
        }
    }
}
