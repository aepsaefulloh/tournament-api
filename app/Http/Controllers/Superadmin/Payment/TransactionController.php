<?php

namespace App\Http\Controllers\Superadmin\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class TransactionController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_transaction';
        $this->table_transaction_detail = 'tbl_transaction_dtl';
        $this->table_prov = 'tbl_prov';
        $this->table_kab = 'tbl_kab';
        $this->table_stock = 'tbl_stock';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $transactions = $this->master->results_filtering(
                $this->table,
                null, 
                ['ID', 'TRID', 'FULLNAME', 'TRDATE', 'TOTAL', 'STATUS'],
                'ID',
                'desc'
            );    

            if (count($transactions) > 0) {
                foreach ($transactions as $key => $transaction) {
                    // Find Transaction Detail 
                    $find_transaction_detail = $this->master->results_filtering($this->table_transaction_detail, 
                        ["TRID" => $transaction->ID], 
                        ["ID", "CODE", \DB::raw('(CASE WHEN SIZE = 1 THEN "S" WHEN SIZE = 2 THEN "M" WHEN SIZE = 3 THEN "L" WHEN SIZE = 4 THEN "XL" END) AS SIZE'), "QTY"],
                        'ID',
                        'asc'
                    );

                    $results[$key] = [
                        "ID"                => $transaction->ID,
                        "TRID"              => $transaction->TRID,
                        "FULLNAME"          => $transaction->FULLNAME,
                        "TRDATE"            => $transaction->TRDATE,
                        "ITEMS"             => (count($find_transaction_detail) > 0) ? $find_transaction_detail : [],
                        "TOTAL"             => $transaction->TOTAL,
                        "STATUS"            => $transaction->STATUS
                    ]; 
                }
            } else {
                $results = [];
            }
            

            return $this->response_data(
                'Result Data Transaction Success!', 
                ['self' => url($request->fullURL())], 
                $results
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $transaction = $this->master->result_filtering(
            $this->table, 
            ['ID' => (int) $id], 
            ['ID', 'TRID', 'FULLNAME', 'ADDRESS', 'PHONE', 'EMAIL', 'PROV', 'KAB', 'KODEPOS', 'SHIPMENT', 'SHIPPING', 'TOTAL', 'STATUS'],
        );

        if ($transaction) {
            // Find Product Detail Transaction
            $find_transaction_detail = $this->master->results_filtering($this->table_transaction_detail, 
                ["TRID" => $transaction->ID], 
                ["ID", "CODE", \DB::raw('(CASE WHEN SIZE = 1 THEN "S" WHEN SIZE = 2 THEN "M" WHEN SIZE = 3 THEN "L" WHEN SIZE = 4 THEN "XL" END) AS SIZE'), "QTY"],
                'ID',
                'asc'
            );

            // Find Province
            $find_province = $this->master->result_filtering(
                $this->table_prov, 
                ['ID' => $transaction->PROV], 
                ['PROV'],
            );

            // Find Kab
            $find_kab = $this->master->result_filtering(
                $this->table_kab, 
                ['ID' => $transaction->KAB], 
                ['KAB'],
            );

            // Result Object Data Transaction
            $result = [
                "ID"            => $transaction->ID,
                "TRID"          => $transaction->TRID, 
                "FULLNAME"      => $transaction->FULLNAME, 
                "ADDRESS"       => $transaction->ADDRESS, 
                "PHONE"         => $transaction->PHONE, 
                "EMAIL"         => $transaction->EMAIL, 
                "PROV"          => (isset($find_province)) ? $find_province->PROV : null, 
                "KAB"           => (isset($find_kab)) ? $find_kab->KAB : null, 
                "KODEPOS"       => $transaction->KODEPOS, 
                "SHIPMENT"      => $transaction->SHIPMENT, 
                "SHIPPING"      => $transaction->SHIPPING, 
                "ITEMS"         => (isset($find_transaction_detail) && count($find_transaction_detail) > 0) ? $find_transaction_detail : [],
                "TOTAL"         => $transaction->TOTAL, 
                "STATUS"        => $transaction->STATUS
            ];

            return $this->response_data(
                'Result Data Transaction Success!', 
                ['self' => url(Request()->fullURL())], 
                $result
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        // Validasi Request
        if ($request->status == '') {
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Transaction Master
                $update_transaction = $this->master->updates($this->table,
                    ["ID" => $request->id],
                    [
                        "FULLNAME"      => $request->fullname,
                        "ADDRESS"       => $request->address,
                        "PHONE"         => $request->phone,
                        "EMAIL"         => $request->email,
                        "KODEPOS"       => $request->kodepos,
                        "SHIPMENT"      => $request->courier,
                        "SHIPPING"      => $request->shipping,
                        "STATUS"        => $request->status
                    ]
                );

                // Update Data Transaction Detail
                $update_transaction_detail = $this->master->updates($this->table_transaction_detail,
                    ["TRID" => $request->id],
                    ["STATUS"        => $request->status]
                );

                // find Product Detail
                $find_transaction_detail = $this->master->results_filtering(
                    $this->table_transaction_detail,
                    ['TRID' => $request->id], 
                    ['ID', 'CODE', 'QTY', 'SIZE'],
                    'ID',
                    'desc'
                );    

                if (count($find_transaction_detail) > 0) {
                    foreach ($find_transaction_detail as $key => $td) {
                        // Get Stock Product
                        $stock = $this->master->result_filtering(
                            $this->table_stock, 
                            [['CODE', $td->CODE], ['SIZE', $td->SIZE]], 
                            ['TOTAL'],
                        );

                        // Update Stock Product
                        $update_stock = $this->master->updates($this->table_stock,
                            [["CODE", $td->CODE], ["SIZE", $td->SIZE]],
                            ["TOTAL"  => ($stock) ? $stock->TOTAL - $td->QTY : 0]
                        );   
                    }
                } 

                if ($update_transaction == true && $update_transaction_detail == true) {
                    return $this->response_message(
                        'Data Transaction Success Updated!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Transaction Failed Updated!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                return $this->response_message(
                    'Not Found!', 
                    ['self' => url($request->fullURL())], 
                    404
                );
            }
        }
    }
}
