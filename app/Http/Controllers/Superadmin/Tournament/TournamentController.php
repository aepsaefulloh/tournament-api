<?php

namespace App\Http\Controllers\Superadmin\Tournament;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class TournamentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_tournament';
        $this->destination = 'storage/images/tournaments/';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $tournaments = $this->master->results_filtering(
                $this->table,
                [['STATUS', '<>', 99]], 
                ['ID', 'TNAME', 'DESCRIPTION', 'STARTDATE', 'TQUOTA', 'TTYPE', 'LOCATION', 
                \DB::raw("(SELECT GAME FROM tbl_game WHERE ID = GAME) AS GAME"), 
                \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'CONTESTANT', 'STATUS'],
                'ID',
                'desc'
            );  
            

            return $this->response_data(
                'Result Data Tournament Success!', 
                ['self' => url($request->fullURL())], 
                $tournaments
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $tournament = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            ['ID', 'TNAME', 'DESCRIPTION', 'STARTDATE', 'TQUOTA', 'TTYPE', 'LOCATION', 
            \DB::raw("(SELECT GAME FROM tbl_game WHERE ID = GAME) AS GAME"), 
            \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'CONTESTANT', 'STATUS'],
        );

        if ($tournament) {
            return $this->response_data(
                'Result Data Tournament Success!', 
                ['self' => url(Request()->fullURL())], 
                $tournament
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        $validator = $this->validate($request, [
            'name' => 'required',
            'start_date' => 'required',
            'quota' => 'required',
            'type' => 'required',
            'location' => 'required',
            'game' => 'required',
            'contestant' => 'required'
        ]);

        $values = [
            'TNAME' => $request->name,
            'STARTDATE' => $request->start_date,
            'TQUOTA' => $request->quota,
            'TTYPE' => $request->type,
            'LOCATION' => $request->location,
            'GAME' => $request->game,
            'CONTESTANT' => $request->contestant
        ];
        
        if ($request->id != '') {
            $values['STATUS'] = $request->status;
            $search_image_old = $this->master->result_filtering($this->table, ['ID' => $request->id], ['IMAGE']);
            
            if ($request->image['name'] != '') {
                $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                
                if ($search_image_old->IMAGE != '') {
                    $this->delete_file($this->destination.$search_image_old->IMAGE);
                }
                $values['IMAGE'] = $imageName;
            } elseif ($search_image_old->IMAGE != '') {
                $imageName = $search_image_old->IMAGE;
                $values['IMAGE'] = $imageName;
            }

            $update_tournament = $this->master->updates($this->table,
                ["ID" => $request->id],
                $values
            );

            if ($update_tournament == true) {
                return $this->response_message(
                    'Data Tournament Success Updated!', 
                    ['self' => url($request->fullURL())], 
                    200
                );
            } else {
                return $this->response_message(
                    'Data Tournament Failed Updated!', 
                    ['self' => url($request->fullURL())], 
                    404
                );
            }
        } else {
            $values['STATUS'] = 1;
            if ($request->image['name'] != '') {
                $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                $values['IMAGE'] = $imageName;
            } else {
                $imageName = 'default.png';
            }

            // Create Data Tournament
            $insert_tournament = $this->master->create($this->table, $values);

            if ($insert_tournament == true) {
                return $this->response_message(
                    'Data Tournament Success Created!', 
                    ['self' => url($request->fullURL())], 
                    200
                );
            } else {
                return $this->response_message(
                    'Data Tournament Failed Created!', 
                    ['self' => url($request->fullURL())], 
                    404
                );
            }
        }
    }

    public function upload_image_content(Request $request) {
        if ($request->image != '') {
            // Save Image From Directory
            $imageName = uniqid().'-'.$this->now_date('Y-m-d') . '.'. $this->ext_base64($request->image);
            $this->save_image($request->image, $imageName, $this->destination, $this->ext_base64($request->image)); 
            
            return $this->response_data(
                'Upload Image Success!', 
                ['self' => url($request->fullURL())], 
                ['IMAGE' => url($this->destination.$imageName)]
            ); 
        } else {
            return $this->response_message(
                'Upload Image Failed!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
        
    }
}
