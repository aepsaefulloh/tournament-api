<?php

namespace App\Http\Controllers\Superadmin\Project;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class ProjectController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_project';
        $this->destination = 'storage/images/projects/';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $projects = $this->master->results_filtering(
                $this->table,
                [['STATUS', '<>', 99]], 
                ['ID', 'TITLE', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'CATEGORY', 'CREATE_TIMESTAMP', 'STATUS'],
                'CREATE_TIMESTAMP',
                'desc'
            );    

            if (count($projects) > 0) {
                foreach ($projects as $key => $project) {
                    // Find Category 
                    $find_category = $this->master->result_filtering('tbl_category', ["ID" => $project->CATEGORY], ["CATEGORY"]);

                    $result[$key] = [
                        "ID"                => $project->ID,
                        "TITLE"             => $project->TITLE,
                        "IMAGE"             => $project->IMAGE,
                        "CATEGORY"          => $find_category->CATEGORY,
                        "CREATE_TIMESTAMP"  => $project->CREATE_TIMESTAMP,
                        "STATUS"            => $project->STATUS
                    ]; 
                }
            } else {
                $result = [];
            }
            

            return $this->response_data(
                'Result Data Project Success!', 
                ['self' => url($request->fullURL())], 
                $result
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $project = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            [ "TITLE", "SUMMARY", "CONTENT", \DB::raw("CONCAT('".url($this->destination)."', '/', IMAGE) AS IMAGE"), "CATEGORY", "KEYWORD", "CREATE_TIMESTAMP", "CREATE_BY", "STATUS", "SUBTITLE"],
        );

        if ($project) {
            return $this->response_data(
                'Result Data Project Success!', 
                ['self' => url(Request()->fullURL())], 
                $project
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        // Validasi Request
        if ($request->title == '' && $request->category == '' && $request->status != '') {
            return $this->response_message(
                "Judul, Category & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->title == '') { 
            return $this->response_message(
                "Judul tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->category == '') { 
            return $this->response_message(
                "Category tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') { 
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Banner
                $search_image_old = $this->master->result_filtering($this->table, ['ID' => $request->id], ['IMAGE']);

                if ($search_image_old) {
                    if ($request->image['name'] != '') {
                        // Save Image
                        $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                        $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                        
                        // Delete Image
                        $this->delete_file($this->destination.$search_image_old->IMAGE);
                    } else {
                        $imageName = $search_image_old->IMAGE;
                    }
    
                    $update_project = $this->master->updates($this->table,
                        ["ID" => $request->id],
                        [
                            "TITLE"             => $request->title,
                            "SUMMARY"           => $request->summary,
                            "CONTENT"           => $request->project,
                            "IMAGE"             => $imageName,
                            "KEYWORD"           => $request->keyword,
                            "CATEGORY"          => $request->category,
                            "CREATE_BY"         => $request->creator,
                            "STATUS"            => $request->status,
                            "SUBTITLE"          => $request->subtitle
                        ]
                    );

                    if ($update_project == true) {
                        return $this->response_message(
                            'Data Project Success Updated!', 
                            ['self' => url($request->fullURL())], 
                            200
                        );
                    } else {
                        return $this->response_message(
                            'Data Project Failed Updated!', 
                            ['self' => url($request->fullURL())], 
                            404
                        );
                    }
                } else {
                    return $this->response_message(
                        'Data Project Not Found!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                if ($request->image['name'] != '') {
                    $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                    $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                } else {
                    $imageName = 'default.png';
                }

                // Create Data Project
                $insert_project = $this->master->create($this->table, [
                    "TITLE"             => $request->title,
                    "SUMMARY"           => $request->summary,
                    "CONTENT"           => $request->project,
                    "IMAGE"             => $imageName,
                    "KEYWORD"           => $request->keyword,
                    "CATEGORY"          => $request->category,
                    "CREATE_TIMESTAMP"  => $this->now_date('Y-m-d G:i:s'),
                    "CREATE_BY"         => $request->creator,
                    "STATUS"            => $request->status,
                    "SUBTITLE"          => $request->subtitle
                ]);
    
                if ($insert_project == true) {
                    return $this->response_message(
                        'Data Project Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Project Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }

    public function upload_image_project(Request $request) {
        if ($request->image != '') {
            // Save Image From Directory
            $imageName = uniqid().'-'.$this->now_date('Y-m-d') . '.'. $this->ext_base64($request->image);
            $this->save_image($request->image, $imageName, $this->destination, $this->ext_base64($request->image)); 
            
            return $this->response_data(
                'Upload Image Success!', 
                ['self' => url($request->fullURL())], 
                ['IMAGE' => url($this->destination.$imageName)]
            ); 
        } else {
            return $this->response_message(
                'Upload Image Failed!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
        
    }
}   
