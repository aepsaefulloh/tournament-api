<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\File;


class Controller extends BaseController
{
    private $allowed_img_ext = ['jpg', 'jpeg', 'png'];
    
    public function response_data($message, $url, $data, $code=200) {
        return response()->json([
            "status"    => $code,
            "message"   => $message,
            "links"     => $url,
            "data"      => $data
        ]);
    }

    public function response_message($message, $url, $code=200) {
        return response()->json([
            "status"    => $code,
            "message"   => $message,
            "links"     => $url
        ]);
    }

    public function Save_image($imageFile, $imageName, $destination, $format_file, $max_size = NULL) {
        $image = str_replace("[removed]", "", $imageFile);
        $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
        $decode_image = base64_decode($img);

        $is_file_validated = self::validateFile($decode_image, $format_file, $max_size, $this->allowed_img_ext);

        if (isset($is_file_validated['status'])) {
            return $is_file_validated;
        }
        
        $make_image = Image::make($decode_image)->save($destination . $imageName);
        return true;
    }

    private static function validateFile($decode_image, $format_file, $max_size, $allowed_img_ext) {    
        if (!in_array(strtolower($format_file), $allowed_img_ext)) {
            return [
                "status" => 422,
                "message" => "File Format Not Allowed"
            ];
        }

        if ($max_size) {
            $is_size_greater = self::checkFileSize($decode_image, $format_file, $max_size);
        }else{
            $is_size_greater = self::checkFileSize($decode_image, $format_file);
        }
        
        if ($is_size_greater) {
            $max_size = ($max_size ?? 10485760);
            $format_bytes = formateBytes($max_size);

            return [
                "status"    => 422,
                "message"   => "File Size is Greater than $format_bytes"
            ];
        }

        return true;
    }

    private static function checkFileSize($decode_image, $format_file, $max_size=10485760) {
        $temp_path = "storage/temp/".time().",".$format_file;
        file_put_contents($temp_path, $decode_image);
        $image_size = filesize($temp_path);
        unlink($temp_path);
        if ($image_size > $max_size) {
            return true;
        }
        return false;
    }

    public static function formateBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');   

        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }

    // -------------------------------- Get Extentions File from base64 code -------------------------------- //
    public function ext_base64($code) {
        $img = explode(',', $code);
        $ini =substr($img[0], 11);
        $type = explode(';', $ini);
        return $type[0];  
    }

    // ------------------------------- Delete File ---------------------------------------------------------- //
    public function delete_file($destination) {
        // Delete File
        $delete = File::delete($destination);
    }

    // ------------------------------------- Date Time ------------------------------------------------------ //
    public function now_date($format) {
        return \Carbon\Carbon::now()->format($format);
    }

    public static function responseStore($is_stored, $storing)
    {
        if ($is_stored) {
            return response()->json([
                "status"    => 200,
                "message"   => "Success Storing $storing!",
            ], 200);
        }else{
            return response()->json([
                "status"    => 500,
                "message"   => "Failde Storing $storing!",
            ], 500);
        }
    }

    public static function responseGet($data, $getting)
    {
        if($data){
            return response()->json([
                'code' => 200,
                'data' => [
                    'message' => "success get $getting data",
                    'data' => $data
                ]
            ], 200);
        }else{
            return response()->json([
                'code' => 404,
                'data' => [
                    'message' => "$getting data not found",
                ]
            ], 404);            
        }
    }
}
