<?php

namespace App\Http\Controllers\Superadmin\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class ContentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_content';
        $this->destination = 'storage/images/contents/';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $contents = $this->master->results_filtering(
                $this->table,
                [['STATUS', '<>', 99]], 
                ['ID', 'TITLE', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'CATEGORY', 'CREATE_TIMESTAMP', 'STATUS'],
                'CREATE_TIMESTAMP',
                'desc'
            );    

            if (count($contents) > 0) {
                foreach ($contents as $key => $content) {
                    // Find Category 
                    $find_category = $this->master->result_filtering('tbl_category', ["ID" => $content->CATEGORY], ["CATEGORY"]);

                    $result[$key] = [
                        "ID"                => $content->ID,
                        "TITLE"             => $content->TITLE,
                        "IMAGE"             => $content->IMAGE,
                        "CATEGORY"          => $find_category->CATEGORY,
                        "CREATE_TIMESTAMP"  => $content->CREATE_TIMESTAMP,
                        "STATUS"            => $content->STATUS
                    ]; 
                }
            } else {
                $result = [];
            }
            

            return $this->response_data(
                'Result Data Content Success!', 
                ['self' => url($request->fullURL())], 
                $result
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $content = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            [ "TITLE", "SUMMARY", "CONTENT", \DB::raw("CONCAT('".url($this->destination)."', '/', IMAGE) AS IMAGE"), "VIDEO", "YOUTUBE", "KEYWORD", "CATEGORY", "SEO", "XURL", "CREATE_TIMESTAMP", "CREATE_BY", "STATUS", "ARTIST", "SPORTIFY", "SUBTITLE"],
        );

        if ($content) {
            return $this->response_data(
                'Result Data Content Success!', 
                ['self' => url(Request()->fullURL())], 
                $content
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        // Validasi Request
        if ($request->title == '' && $request->category == '' && $request->status != '') {
            return $this->response_message(
                "Judul, Category & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->title == '') { 
            return $this->response_message(
                "Judul tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->category == '') { 
            return $this->response_message(
                "Category tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') { 
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Banner
                $search_image_old = $this->master->result_filtering($this->table, ['ID' => $request->id], ['IMAGE']);

                if ($search_image_old) {
                    if ($request->image['name'] != '') {
                        // Save Image
                        $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                        $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                        
                        // Delete Image
                        $this->delete_file($this->destination.$search_image_old->IMAGE);
                    } else {
                        $imageName = $search_image_old->IMAGE;
                    }
    
                    $update_content = $this->master->updates($this->table,
                        ["ID" => $request->id],
                        [
                            "TITLE"             => $request->title,
                            "SUMMARY"           => $request->summary,
                            "CONTENT"           => $request->content,
                            "IMAGE"             => $imageName,
                            "VIDEO"             => $request->video_project,
                            "YOUTUBE"           => $request->youtube_url,
                            "KEYWORD"           => $request->keyword,
                            "CATEGORY"          => $request->category,
                            "SEO"               => $request->seo,
                            "XURL"              => $request->external_url, 
                            "CREATE_BY"         => $request->creator,
                            "STATUS"            => $request->status,
                            "ARTIST"            => $request->artist,   
                            "SPORTIFY"          => $request->link_sportify,
                            "SUBTITLE"          => $request->subtitle
                        ]
                    );

                    if ($update_content == true) {
                        return $this->response_message(
                            'Data Content Success Updated!', 
                            ['self' => url($request->fullURL())], 
                            200
                        );
                    } else {
                        return $this->response_message(
                            'Data Content Failed Updated!', 
                            ['self' => url($request->fullURL())], 
                            404
                        );
                    }
                } else {
                    return $this->response_message(
                        'Data Content Not Found!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                if ($request->image['name'] != '') {
                    $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . rand() . '.'. $request->image['type'];
                    $this->save_image($request->image['name'], $imageName, $this->destination, $request->image['type']);
                } else {
                    $imageName = 'default.png';
                }

                // Create Data Content
                $insert_content = $this->master->create($this->table, [
                    "TITLE"             => $request->title,
                    "SUMMARY"           => $request->summary,
                    "CONTENT"           => $request->content,
                    "IMAGE"             => $imageName,
                    "VIDEO"             => $request->video_project,
                    "YOUTUBE"           => $request->youtube_url,
                    "KEYWORD"           => $request->keyword,
                    "CATEGORY"          => $request->category,
                    "SEO"               => $request->seo,
                    "XURL"              => $request->external_url, 
                    "CREATE_TIMESTAMP"  => $this->now_date('Y-m-d G:i:s'),
                    "CREATE_BY"         => $request->creator,
                    "STATUS"            => $request->status,
                    "ARTIST"            => $request->artist,   
                    "SPORTIFY"          => $request->link_sportify,
                    "SUBTITLE"          => $request->subtitle
                ]);
    
                if ($insert_content == true) {
                    return $this->response_message(
                        'Data Content Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Content Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }

    public function upload_image_content(Request $request) {
        if ($request->image != '') {
            // Save Image From Directory
            $imageName = uniqid().'-'.$this->now_date('Y-m-d') . '.'. $this->ext_base64($request->image);
            $this->save_image($request->image, $imageName, $this->destination, $this->ext_base64($request->image)); 
            
            return $this->response_data(
                'Upload Image Success!', 
                ['self' => url($request->fullURL())], 
                ['IMAGE' => url($this->destination.$imageName)]
            ); 
        } else {
            return $this->response_message(
                'Upload Image Failed!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
        
    }
}   
