<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use DB;

class Users extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    
    public function __construct() {
        $this->table = 'tbl_user';
    }

    public function datatables($group_id = NULL) {
        $account = DB::table($this->table)
            ->select('ID', 'USERNAME', 'EMAIL', 'ID_GROUP', 'LASTLOGIN', 'STATUS')
            ->when($group_id, function ($query) use ($group_id) {
                return $query->where('ID_GROUP', $group_id);
            })->get();

        if (count($account) > 0) {
            foreach ($account as $key => $act) {
                // Get Role Account 
                $role = DB::table('tbl_group')->where('ID', $act->ID_GROUP)->first();

                // Result Account
                $result[$key] = [
                    "ID"            => $act->ID,
                    "USERNAME"      => $act->USERNAME,
                    "EMAIL"         => $act->EMAIL,
                    "GROUP"         => [
                        "ID"    => $role->ID,
                        "NAME"  => $role->GROUP_NAME
                    ],
                    "LASTLOGIN"     => $act->LASTLOGIN,
                    "STATUS"        => $act->STATUS
                ];
            }
        } else {
            $result = [];
        }

        return $result;
    }

    public static function getIdFromToken($token)
    {
        $user = self::where('TOKEN', $token)->first();

        if (isset($user->ID)) {
            return $user->ID;
        } else {
            return false;
        }
    }
}
