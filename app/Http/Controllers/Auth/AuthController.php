<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Auth;
use App\Models\Master;

class AuthController extends Controller
{
    public function __construct() {
        $this->master = New Master;
        $this->auth = New Auth;
        $this->table_auth = 'tbl_user';
        $this->table_logs = 'tbl_log';
    }

    public function login(Request $request) {
        // Validasi Request an Jika Null Username & Password
        if ($request->username == '' && $request->password == '') {
            return $this->response_message(
                "Username & Password tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->username == '') {
            return $this->response_message(
                "Username tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->password == '') {
            return $this->response_message(
                "Password tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            $account = $this->auth->login(
                $this->table_auth, 
                $request->username, 
                $request->password
            );

            if ($account['status'] == true) {
                // Insert Logs
                $insert_logs = $this->master->create($this->table_logs, [
                    'PROC'          => 'login',
                    'ACC'           => $account['data']['USERNAME'],
                    'TBL'           => 'tbl_user',
                    'REMARKS'       => $request->header('user_agent'),
                    'LOG_TIMESTAMP' => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);

                return $this->response_data(
                    "Login Success", 
                    ["self" => url($request->fullURL())], 
                    $account['data']
                );   
            } else {
                return $this->response_message(
                    "Akun tidak ditemukan!", 
                    ["self" => url($request->fullURL())], 
                    401
                );
            }
        }
    }

    public function logout($id) {
        $account = $this->master->result_filtering($this->table_auth, ['ID' => $id], ['ID', 'TOKEN']);
        
        if ($account) {
            // Remove Token Admin
            $token_null = $this->master->updates($this->table_auth, ['ID' => $account->ID], ['TOKEN' => null]);
            
            return $this->response_message(
                "Logout Success", 
                ['self' => url(Request()->fullURL())], 
                200
            );
        } else {
            return $this->response_message(
                "Logout Failed!", 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function logs($id) {
        // Find Account User
        $account = $this->master->result_filtering($this->table_auth, ['ID' => $id], ['ID', 'USERNAME']);

        if ($account) {
            $logs = $this->master->results_filtering(
                $this->table_logs,
                [
                    ['TBL', '=', 'tbl_user'],
                    ['ACC', '=', $account->USERNAME]
                ],
                ['ID', 'ACC', 'LOG_TIMESTAMP'],
                'LOG_TIMESTAMP',
                'desc'
            );

            return $this->response_data(
                'Result Data Logs Success!', 
                ['self' => url(Request()->fullURL())], 
                $logs
            );
        } else {
            return $this->response_message(
                "Not Found!", 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }
}
