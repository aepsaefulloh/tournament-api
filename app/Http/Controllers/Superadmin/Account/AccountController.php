<?php

namespace App\Http\Controllers\Superadmin\Account;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Master;

class AccountController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->users = New Users;
        $this->master = New Master;
        $this->table = 'tbl_user';
    }

    public function result_all(Request $request) {
        if ($request->type == 'table') {
            $account = $this->users->datatables($request->group_id);

            return $this->response_data(
                'Result Data Account User Success!', 
                ['self' => url($request->fullURL())], 
                $account
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $account = $this->master->result_filtering(
            $this->table,
            ['ID' => $id], 
            ['ID', 'USERNAME', 'EMAIL', 'FULLNAME','LASTLOGIN','PASSWD', 'PHONE', 'ID_GROUP', 'STATUS']
        );

        if ($account) {
            return $this->response_data(
                'Result Data Account User Success!', 
                ['self' => url(Request()->fullURL())], 
                $account
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        if ($request->email == '' && $request->username == '' && $request->password == '' && $request->fullname == '' && $request->role == '' && $request->status == '') {
            return $this->response_message(
                "Email, Username, Nama Lengkap, Password, Role & Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->email == '') {
            return $this->response_message(
                "Email tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->username == '') {
            return $this->response_message(
                "Username tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->fullname == '') {
            return $this->response_message(
                "Nama Lengkap tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->password == '' && $request->password_old == '') {
            return $this->response_message(
                "Password tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->role == '') {
            return $this->response_message(
                "Role tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->role == '') {
            return $this->response_message(
                "Role tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data User
                if ($request->password != '') {
                    $update_user = $this->master->updates(
                        $this->table,
                        ['ID' => $request->id], 
                        [
                            'EMAIL'     => $request->email,
                            'USERNAME'  => $request->username,
                            'FULLNAME'  => $request->fullname,
                            'PASSWD'    => Hash::make($request->password), 
                            'PHONE'     => $request->phone, 
                            'JOB'       => $request->job,
                            'ID_GROUP'  => $request->role,
                            'STATUS' => $request->status
                        ]
                    );
                } else {
                    $update_user = $this->master->updates(
                        $this->table,
                        ['ID' => $request->id], 
                        [
                            'EMAIL'     => $request->email,
                            'USERNAME'  => $request->username,
                            'FULLNAME'  => $request->fullname,
                            'PHONE'     => $request->phone, 
                            'JOB'       => $request->job,
                            'ID_GROUP'  => $request->role,
                            'STATUS' => $request->status
                        ]
                    );
                }
                
                if ($update_user == true) {
                    return $this->response_message(
                        'Data Account User Success Updated!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Account User Failed Updated!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                // Create Data User
                $insert_user = $this->master->create($this->table, [
                        'EMAIL'     => $request->email, 
                        'USERNAME'  => $request->username,
                        'FULLNAME'  => $request->fullname,
                        'PASSWD'    => Hash::make($request->password),
                        'JOB'       => $request->job,
                        'PHONE'     => $request->phone,
                        'ID_GROUP'  => $request->role,
                        'STATUS'    => $request->status,
                        'CREATED'   => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);
    
                if ($insert_user == true) {
                    return $this->response_message(
                        'Data Account User Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Account User Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }
}
