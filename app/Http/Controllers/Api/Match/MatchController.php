<?php

namespace App\Http\Controllers\Api\Match;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class MatchController extends Controller
{
    public function __construct()
    {
        $this->middleware('api_client');
        $this->table = 'tbl_match';
        $this->master = new Master;
        $this->destination = 'storage/images/contents/';
    }

    public function result_all_jurnal_playlist(Request $request)
    {
        $limit = ($request->limit != '') ? $request->limit : null;
        $content = $this->master->result_content_journal_playlist(
            $this->table,
            [
                'ID', \DB::raw("CONCAT('" . url($this->destination) . "', '/', IMAGE) AS IMAGE"),
                'TITLE', 'SUMMARY', 'SUBTITLE', 'CREATE_BY', 'CREATE_TIMESTAMP',
                \DB::raw("(SELECT CATEGORY FROM tbl_category WHERE ID = $this->table.CATEGORY) AS CATEGORY")
            ],
            $limit
        );

        return $this->response_data(
            'Result Data Content Success!',
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "limit"     => $limit
                ],
            ],
            $content
        );
    }

    public function result_all_match(Request $request)
    {
        $page = ($request->page != '') ? $request->page : 1;
        $perpage = ($request->perpage) ? $request->perpage : 10;
        $limit = ($request->limit) ? $request->limit : 10;
        $where = [
            ['STATUS', 1],
        ];
        if ($request->category != '') {
            $where[] = ['CATEGORY', $request->category];
        }
        $content = $this->master->result_all_match(
            $this->table,
            $limit,
        );

        // Count Data All
        $count_data = $this->master->count_result_content(
            $this->table,
            $where,
        );

        return $this->response_data(
            'Result Data Content Success!',
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "count_data" => $count_data,
                    "page"      => $page,
                    "perpage"   => $perpage
                ],
            ],
            $content
        );
    }

    public function result_find($id)
    {
        $content = $this->master->result_find_content(
            $this->table,
            ['ID_ARTICLE' => $id],
        );

        return $this->response_data(
            'Result Data Content Success!',
            [
                'self'          => url(Request()->fullURL()),
            ],
            $content
        );
    }

    public function store(Request $request)
    {
        $insert_comment = $this->master->create($this->table, [
            'EMAIL'         => $request->EMAIL,
            'ARTICLE_ID'    => $request->ARTICLE_ID,
            'USER'          => $request->FULLNAME,
            'COMMENT'       => $request->COMMENT,
            'CTIMESTAMP'    => $request->CTIMESTAMP,
            'IPLOC'         => $request->IPLOC,
            'STATUS'        => 1,
        ]);

        if ($insert_comment) {
            return $this->response_message(
                'Comment success!',
                ['self' => url($request->fullURL())],
                200
            );
        }
    }
}
