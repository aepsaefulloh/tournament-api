<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Master;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
        $this->master = New Master;
        $this->table = 'tbl_user';
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // Searching Token in Table User
        // if ($request->access_token != '') {
        //     $account = $this->master->result_filtering($this->table, [['TOKEN', $request->access_token]], ['TOKEN']);
        // }
        
        // Cek Token Akun
        if ($request->access_token == env('ACCESS_TOKEN')) {
            return $next($request);
        } else {
            return response(['status'=>401,'message'=>'Unauthorized.'], 401);   
        }
    }
}
