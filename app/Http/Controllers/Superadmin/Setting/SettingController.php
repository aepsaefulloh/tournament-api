<?php

namespace App\Http\Controllers\Superadmin\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_config';
        $this->destination = 'storage/images/apps/';
    }

    public function results_filtering(Request $request) {
        $config = $this->master->results_filtering(
            $this->table,
            [['STATUS', 1]], 
            ['ID', 'CATEGORY', 'LABEL', 'CKEY', \DB::raw("(CASE WHEN CKEY= 'DD_LOGO' THEN CONCAT('".url($this->destination)."', '/', CVALUE) ELSE CVALUE END) as CVALUE"), 'CTYPE', 'ORDNUM', 'STATUS'],
            'ORDNUM',
            'asc'
        );    

        return $this->response_data(
            'Result Data Config Apps Success!', 
            ['self' => url($request->fullURL())], 
            $config
        );
    }

    public function store(Request $request) {
        // Get Request All
        $data = $request->all();

        // Validasi Request
        if (count($data) > 0) {   
            // Vaidasi Jika Image Diupdate
            if ($data['DD_LOGO']['NAME'] != '' && $data['DD_LOGO']['TYPE'] != '') {
                // Find Image Old
                $search_image_old = $this->master->result_filtering($this->table, ['CKEY' => 'DD_LOGO'], ['CVALUE']);

                // Save Image
                $imageName = rand().'-'.$this->now_date('Y-m-d') . '-' . $data['DD_SITENAME'][1] . '.'. $data['DD_LOGO']['TYPE'];
                $this->save_image($data['DD_LOGO']['NAME'], $imageName, $this->destination, $data['DD_LOGO']['TYPE']);
                
                $update_config_image = $this->master->updates($this->table,
                    [['CKEY', 'DD_LOGO']],
                    ['CVALUE' => $imageName]  
                );

                // Delete Image
                $this->delete_file($this->destination.$search_image_old->CVALUE);
            } 

            // Remove Object Logo In Array
            unset($data['DD_LOGO']);
            
            // Insert / Update Data Setting Apps
            foreach ($data as $value) {
                $update_config = $this->master->updates($this->table,
                    [['ID', $value[0]]],
                    ['CVALUE' => $value[1]]  
                );
            }

            return $this->response_message(
                'Data Setting Apps Success Updated!', 
                ['self' => url($request->fullURL())], 
                200
            );
        } 
    }
}
