<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Auth as AuthModel;
use App\Mail\SendResetPassword;
use App\Mail\SendVerification;
use App\Models\ResetPassword;
use Illuminate\Http\Request;
use App\Libs\CustomResponse;
use App\Rules\WithoutSpace;
use App\Models\Master;
use App\Models\Users;
use DB;

class AuthController extends Controller
{
    private static $image_path = 'storage/images/members/';

    public function __construct() {
        $this->master = New Master;
        $this->auth = New AuthModel;
        $this->table_auth = 'tbl_user';
        $this->table_logs = 'tbl_log';
    }

    public function login(Request $request) {
        $validator = $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);
        
        $account = $this->auth->login(
            $this->table_auth, 
            $request->username, 
            $request->password
        );

        if ($account['status'] == true) {
            return $this->response_data(
                "Login Success", 
                ["self" => url($request->fullURL())], 
                $account['data']
            );   
        } else {
            return $this->response_message(
                "Akun tidak ditemukan!", 
                ["self" => url($request->fullURL())], 
                401
            );
        }
    }
    
    public function register(Request $request) {
        $validator = $this->validate($request, [
            'username' => ['required', 'unique:tbl_user', new WithoutSpace],
            'password' => 'required',
            'email' => 'required|email|unique:tbl_user',
            'fullname' => 'required',
            'city_id' => 'required'
        ]);

        $image = self::$image_path . "11-1.jpg";
        $token = Hash::make(time());

        $insert = [
            'USERNAME'  => $request->username,
            'PASSWD'    => Hash::make($request->password),
            'EMAIL'     => $request->email,
            'FULLNAME'  => $request->fullname,
            'CITY_ID'   => $request->city_id,
            'ID_GROUP'  => 5,
            'TOKEN'     => $token,
            'IMAGE'     => $image
        ];

        $email = [
            "token" => $token,
            "email" => $request->email,
            "name" => $request->fullname
        ];

        $store_user = Users::insertGetId($insert);

        if (!SendVerification::send($email)) return CustomResponse::failed_send_email();
        
        return CustomResponse::store($store_user, "User", [
            "message" => "Email Verification Sent To $request->email"
        ]);
    }

    public function logout($id) {
        $account = $this->master->result_filtering($this->table_auth, ['ID' => $id], ['ID', 'TOKEN']);
        
        if ($account) {
            // Remove Token Admin
            $token_null = $this->master->updates($this->table_auth, ['ID' => $account->ID], ['TOKEN' => null]);
            
            return $this->response_message(
                "Logout Success", 
                ['self' => url(Request()->fullURL())], 
                200
            );
        } else {
            return $this->response_message(
                "Logout Failed!", 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function google(Request $request)
    {
        $data = $request->all();
        $user = Users::where('email', $data['email'])->first();

        if ($user) {
            $data = [
                "ID"        => $user->ID,
                "USERNAME"  => $user->USERNAME,
                "EMAIL"     => $user->EMAIL,
                "FULLNAME"  => $user->FULLNAME,
                "ROLE"      => 5,
                "TOKEN"     => $user->TOKEN,
                "IMAGE"     => $user->IMAGE,
                "IS_LOGIN"  => true
            ];
            return CustomResponse::success($data, "Success Login");
        } else {
            $token = Hash::make(time());
            $username = "User#" . time();
            
            $insert = [
                'USERNAME'  => $username,
                'EMAIL'     => $data['email'],
                'FULLNAME'  => $data['name'],
                'ID_GROUP'  => 5,
                'TOKEN'     => $token,
                'IMAGE'     => $data['picture'],
                'IS_VERIFIED' => true
            ];

            $store_user = Users::insertGetId($insert);
            $data = [
                "ID"        => $store_user,
                "EMAIL"     => $data['email'],
                "FULLNAME"  => $data['name'],
                "USERNAME"  => $username,
                "ROLE"      => 5,
                "TOKEN"     => $token,
                "IMAGE"     => $data['picture'],
                "IS_LOGIN"  => true
            ];
            return CustomResponse::store($store_user, "User", $data);
        }
    }

    public function forgot_password(Request $request)
    {
        $validator = $this->validate($request, [
            'email' => 'required|email'
        ]);

        $hash = Hash::make(time());
        $user = Users::where('EMAIL', $request->email)->first();

        if (isset($user->ID)) {
            $values = [
                'token' => $hash,
                'user_id' => $user->ID,
                'expired_date' => date('Y-m-d H:i:s', strtotime('+1 hours')),
            ];
        }else{
            return $this->response_message(
                "Email not found!", 
                ['self' => url($request->fullURL())], 
                404
            );
        }

        $is_sended = SendResetPassword::send([
            'token' => $hash,
            'name' => $user->FULLNAME,
            'email' => $request->email,
        ]);

        if ($is_sended) {
            $is_stored = ResetPassword::insert($values);
            if ($is_stored) {
                return $this->response_message(
                    "Success Send Email", 
                    ['self' => url($request->fullURL())], 
                    200
                );
            } else {
                return $this->response_message(
                    "Failed Insert To DB!", 
                    ['self' => url($request->fullURL())], 
                    500
                );
            }
        }else{
            return $this->response_message(
                "Failed Send Email!", 
                ['self' => url($request->fullURL())], 
                500
            );
        }
    }

    public function verificate_email(Request $request)
    {
        $user = Users::select('ID', 'EMAIL', 'FULLNAME', 'USERNAME', 'ID_GROUP', 'TOKEN', 'IMAGE')
        ->where('TOKEN', $request->token)
        ->where('IS_VERIFIED', false)
        ->first();

        if (!$user) return CustomResponse::not_found();
        
        $is_updated = Users::where('TOKEN', $request->token)
        ->where('IS_VERIFIED', false)
        ->update(['IS_VERIFIED' => true]);
        
        if (!$is_updated) return CustomResponse::error('Failed Update User Verified');

        $data = [
            "ID"        => $user['ID'],
            "EMAIL"     => $user['EMAIL'],
            "FULLNAME"  => $user['FULLNAME'],
            "USERNAME"  => $user['USERNAME'],
            "ROLE"      => $user['ID_GROUP'],
            "TOKEN"     => $user['TOKEN'],
            "IMAGE"     => $user['IMAGE'],
            "IS_LOGIN"  => true
        ];
        
        return CustomResponse::success($data);
    }
    
    public function reset_password(Request $request)
    {
        $validator = $this->validate($request, [
            'token' => 'required'
        ]);

        $now = date('Y-m-d H:i:s');
        
        $reset_password = ResetPassword::select(
            DB::raw("(SELECT EMAIL FROM tbl_user WHERE ID = user_id) AS email")
        )->where([
            ['token', $request->token],
            ['expired_date', '>=', $now],
            ['is_active', true]
        ])->first();

        if (isset($reset_password->email)) {
            return $this->response_data(
                "Success Get User", 
                ["self" => url($request->fullURL())], 
                $reset_password
            );   
        }else{
            return $this->response_message(
                "User not found!", 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function reset_password_post(Request $request)
    {
        $validator = $this->validate($request, [
            'token' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);
        
        $now = date('Y-m-d H:i:s');

        $reset_password = ResetPassword::where([
            ['token', $request->token],
            ['expired_date', '>=', $now],
            ['is_active', true]
        ])->first();

        if (isset($reset_password->id)) {
            $reset_password->is_active = false;
            $is_updated = $reset_password->save();
            
            if ($is_updated) {
                $is_updated = Users::where('ID', $reset_password->user_id)->update([
                    'PASSWD' => Hash::make($request->password)
                ]);
                
                if ($is_updated) {
                    return $this->response_message(
                        "Success Update Password", 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                }else{
                    return $this->response_message(
                        "Failed Update User Password!", 
                        ['self' => url($request->fullURL())], 
                        500
                    );
                }
            }else{
                return $this->response_message(
                    "Failed Update Token!", 
                    ['self' => url($request->fullURL())], 
                    500
                );
            }
        }else{
            return $this->response_message(
                "User not found!", 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }
}
