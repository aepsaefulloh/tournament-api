<?php

namespace App\Libs;

use \Mailjet\Resources;

class MailJet
{
    public static function send($body) {
        $mj = new \Mailjet\Client(env('MAILJET_KEY'), env('MAILJET_SECRET'), true, ['version' => env('MAILJET_VERSION')]);
        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response->success();
    }
}
