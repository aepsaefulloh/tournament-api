<?php

namespace App\Http\Controllers\Api\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class BannerController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table = 'tbl_banners';
        $this->master = New Master;
        $this->destination = 'storage/images/banners/';
    }

    public function result_about(Request $request) {
        $limit = ($request->limit != '') ? $request->limit : null;

        $about = $this->master->result_banner(
            $this->table, 
            [['POS', 'ABOUT-BANNER'], ['STATUS', 1]], 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', FILENAME) AS FILENAME")], 
            $limit
        );
        
        return $this->response_data(
            'Result Data Banner About Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "limit"     => $limit
                ],
            ], 
            $about
        );
    }
}
