<?php

namespace App\Http\Controllers\Superadmin\Token;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class TokenController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->master = New Master;
        $this->table = 'tbl_token';
    }

    public function results_filtering(Request $request) {
        if ($request->type == 'table') {
            $token = $this->master->results_filtering(
                $this->table,
                [['STATUS', '<>', 99]], 
                ['ID', 'NAME', 'TOKEN', 'CREATE_DATE', 'STATUS'],
                'CREATE_DATE',
                'desc'
            );    

            return $this->response_data(
                'Result Data Token Apps Success!', 
                ['self' => url($request->fullURL())], 
                $token
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url($request->fullURL())], 
                404
            );
        }
    }

    public function result_find($id) {
        $token = $this->master->result_filtering(
            $this->table, 
            ['ID' => $id], 
            ['ID', 'NAME', 'TOKEN', 'CREATE_DATE', 'STATUS'],
        );

        if ($token) {
            return $this->response_data(
                'Result Data Token Apps Success!', 
                ['self' => url(Request()->fullURL())], 
                $token
            );
        } else {
            return $this->response_message(
                'Not Found!', 
                ['self' => url(Request()->fullURL())], 
                404
            );
        }
    }

    public function store(Request $request) {
        if ($request->name == '' && $request->token == '' && $request->status == '' && $request->date == '') {
            return $this->response_message(
                "Name, Token, Status & Date tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->name == '') {
            return $this->response_message(
                "Name tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->token == '') {
            return $this->response_message(
                "Token tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->status == '') {
            return $this->response_message(
                "Status tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } elseif ($request->date == '') {
            return $this->response_message(
                "Date tidak boleh kosong!", 
                ["self" => url($request->fullURL())], 
                401
            );
        } else {
            if ($request->id != '') {
                // Update Data Token
                $update_token = $this->master->updates(
                    $this->table, 
                    ['ID' => $request->id], ['NAME' => $request->name, 'STATUS' => $request->status]
                );
    
                if ($update_token == true) {
                    return $this->response_message(
                        'Data Token Apps Success Updated!', 
                        ['self' => url($request->fullURL())],
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Token Apps Failed Updated!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            } else {
                // Create Data Token
                $insert_token = $this->master->create(
                    $this->table, 
                    ['NAME' => $request->name, 'TOKEN' => $request->token, 'CREATE_DATE' => $request->date, 'STATUS' => $request->status]
                );
    
                if ($insert_token == true) {
                    return $this->response_message(
                        'Data Token Apps Success Created!', 
                        ['self' => url($request->fullURL())], 
                        200
                    );
                } else {
                    return $this->response_message(
                        'Data Token Apps Failed Created!', 
                        ['self' => url($request->fullURL())], 
                        404
                    );
                }
            }
        }
    }
}
