<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use PHPMailer\PHPMailer\PHPMailer;  
use PHPMailer\PHPMailer\Exception;

class PHPMail extends Model
{
    public function sendMail($objItem) {
        // Create an instance of PHPMailer class  
        $mail = new PHPMailer; 
        $mail->SMTPDebug = false;   
        // SMTP configurations 
        $mail->isSMTP(); 
        $mail->Host = env('SMTP_HOST');  				
        $mail->SMTPAuth = true;                 
        $mail->Username = env('SMTP_USER');            
        $mail->Password = env('SMTP_PASS');            
        $mail->SMTPSecure = 'TLS';              
        $mail->Port = env('SMTP_PORT');    
        
        $mail->setFrom(env('SMTP_USER'), env('SENDER_NAME'));
        $mailto=explode(";",$objItem['MAILTO']);
        foreach($mailto as $address){
            $mail->addAddress($address, $objItem['NAME']);     
        }
        
        $mail->addReplyTo(env('SMTP_USER'), env('SENDER_NAME'));
        
        if(isset($objItem['ATTACH'])&& $objItem['ATTACH']!=''){
            $mail->addAttachment($objItem['ATTACH']);         // Add attachments
        }
        $mail->isHTML(true);                                  // Set email format to HTML
        
        $mail->Subject = $objItem['SUBJECT'];
        $mail->Body    = $objItem['BODY'];
        $mail->AltBody = '';
        
        
        $status=false;
        if(!$mail->send()) {
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            $status=true;
        }
        
        return $status;
    }
}
