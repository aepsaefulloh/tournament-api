<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table = 'tbl_config';
        $this->master = New Master;
        $this->destination = 'storage/images/apps/';
    }

    public function result_all(Request $request) {
        $apps = $this->master->result_config(
            $this->table, 
            ['STATUS' => 1], 
            ['LABEL', \DB::raw('CASE WHEN LABEL="Logo" THEN CONCAT("'. url($this->destination) .'", "/", CVALUE) ELSE CVALUE END AS CVALUE')]
        );
        
        
        return $this->response_data(
            'Result Data Config Apps Success!', 
            [
                'self'          => url($request->fullURL()),
            ], 
            $apps
        );
    }
}
