<?php

namespace App\Http\Controllers\Api\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Master;

class ContentController extends Controller
{
    public function __construct() {
        $this->middleware('api_client');
        $this->table = 'tbl_content';
        $this->master = New Master;
        $this->destination = 'storage/images/contents/';
    }

    public function result_all_jurnal_playlist(Request $request) {
        $limit = ($request->limit != '') ? $request->limit : null;
        $content = $this->master->result_content_journal_playlist($this->table, 
        ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 
                'TITLE', 'SUMMARY', 'SUBTITLE','CREATE_BY','CREATE_TIMESTAMP',
                \DB::raw("(SELECT CATEGORY FROM tbl_category WHERE ID = $this->table.CATEGORY) AS CATEGORY")
        ], $limit);
        
        return $this->response_data(
            'Result Data Content Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "limit"     => $limit
                ],
            ], 
            $content
        );
    }

    public function result_all(Request $request) {
        $page = ($request->page != '') ? $request->page : 1;
        $perpage = ($request->perpage) ? $request->perpage : 10 ;
        $where = [
            ['STATUS', 1]
        ];
        if($request->category != '') {
            $where[] = ['CATEGORY', $request->category];
        }
        $content = $this->master->result_content(
            $this->table,
            $where,
            
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 
                'TITLE', 'SUMMARY', 'SUBTITLE','CREATE_BY','CREATE_TIMESTAMP',
                \DB::raw("(SELECT CATEGORY FROM tbl_category WHERE ID = $this->table.CATEGORY) AS CATEGORY")
            ], 
            $page,
            $perpage,
            'ID',
        );
        
        // Count Data All
        $count_data = $this->master->count_result_content(
            $this->table, 
            $where, 
        ); 
        
        return $this->response_data(
            'Result Data Content Success!', 
            [
                'self'          => url($request->fullURL()),
                'parameters'    => [
                    "count_data"=> $count_data,
                    "page"      => $page,
                    "perpage"   => $perpage
                ],
            ], 
            $content
        );
    }

    public function result_find($id) {
        $content = $this->master->result_find_content(
            $this->table, 
            ['ID' => $id], 
            ['ID', \DB::raw("CONCAT('". url($this->destination) ."', '/', IMAGE) AS IMAGE"), 'TITLE', 'SUMMARY', 'CONTENT', 'CREATE_BY', 'CREATE_TIMESTAMP', 'SUBTITLE', 'CATEGORY']
        );
        
        return $this->response_data(
            'Result Data Content Success!', 
            [
                'self'          => url(Request()->fullURL()),
            ], 
            $content
        );
    }
}