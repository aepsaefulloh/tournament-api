<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Users;

class AuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasHeader('token')) {
            $token = $request->header('token');
            $user = Users::where('TOKEN', $token)->first();

            if (!isset($user->ID)) {
                return response(['status' => 401, 'message' => 'Unauthorized.'], 401);
            }
        }else{
            return response(['status' => 400, 'message' => 'Missing Parameter Token'], 400);
        }

        return $next($request);
    }
}
